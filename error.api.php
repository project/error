<?php
/**
 * @file
 * Hooks provided by the Error pages module.
 */

/**
 * Allows module to modify 403 and 404 pages if certain conditions are met.
 */
 function hook_error_alter(&$errors, $uri) {
   switch ( $uri ) {
   	case '/admin' :
   		$errors['403']['title'] = t('You’re not an administrator');
   		$errors['403']['page'] = t('Only administrators can access administration pages.');
   	break;
   }
 }
