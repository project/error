# Error pages

This module takes helps customize 403 (Access denied) and 404 (Page not found) pages by setting its own 403 and 404 paths.

Installing this module doesn't do anything special unless other modules are installed that implement its API.

By default, these pages display Drupal's regular title and content. However, the module also provides a very simple API through which other modules can provide custom 403 and 404 pages whose title and content can depend on any condition.

On installation, the module replaces the system's 403 and 404 settings with its own, and disables the settings' form items. The original settings are put back in place when the module is uninstalled.